from rest_framework.test import APITransactionTestCase
from rest_framework import status
from bert.utils.helpers import fetch_user_feedback, preserve_user_feedback_s3
import json


class BertTests(APITransactionTestCase):

    def setUp(self):
        with open('bert/fixtures/request.json', 'r') as fp:
            self.qna_request = json.loads(fp.read())

    def test_simple_request(self):
        url = "/bert/"
        payload = self.qna_request
        res = self.client.post(url, payload, format='json')

        print(res.json())
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(type(res.json()), list)

    def test_fetch_user_feedback(self):
        items = fetch_user_feedback()
        preserve_user_feedback_s3(items)
