from django.contrib import admin
from django.contrib.auth import get_user_model
from actstream import models

admin.site.unregister(models.Action)

try:
    from genericadmin.admin import GenericAdminModelAdmin as ModelAdmin
except ImportError:
    ModelAdmin = admin.ModelAdmin


User = get_user_model()


@admin.register(models.Action)
class KonoActionAdmin(ModelAdmin):
    list_display = ('actor', 'verb', 'target', 'timestamp')
    search_fields = ['description']
