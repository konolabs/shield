from actstream import action
from django.contrib.auth import get_user_model
from rest_framework import status, viewsets
from rest_framework.response import Response

from bert.utils.helpers import fetch_user_feedback, preserve_user_feedback

from .utils.kono_squad import predict

User = get_user_model()


class BertViewSet(viewsets.ViewSet):

    def list(self, request):
        res = fetch_user_feedback()
        return Response(status=status.HTTP_200_OK, data=dict(ok=True, res=res))

    def post(self, request):
        data = request.data
        user = User.objects.first()
        action.send(user, verb='request', description=data)
        res = predict(data, n_best=False)
        action.send(user, verb='response', description=res)

        context = data['data'][0]['paragraphs'][0]['context']
        preserve_user_feedback(**dict(context=context, res=res))

        return Response(status=status.HTTP_200_OK, data=res)
