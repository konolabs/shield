from constance import config
from django.conf import settings
from slacker import Slacker

KONO_NOTICE_SLACK_API = settings.KONO_NOTICE_SLACK_API
slack = Slacker(KONO_NOTICE_SLACK_API)


def send_slack_message(text='Daily report', message="", channel=None):

    if not KONO_NOTICE_SLACK_API:
        print("Warning : No KONO_NOTICE_SLACK_API")
        return

    if not channel:
        channel = config.CHANNEL_DAILY_REPORT

    slack.chat.post_message(
        channel,
        text="%s\n" % (text),
        attachments=[
            {
                "title": "%s" % message,
                "fallback": "Required plain-text summary of the attachment.",
                "color": "#36a64f",
                "title_link": 'http://nlu-shield.kono.ai/admin/',
            }
        ],
        username='konobot')
