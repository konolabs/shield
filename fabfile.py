import boto3
from fabric.api import local, prefix

from sagemaker.tensorflow.serving import Model

role = 'AmazonSageMaker-ExecutionRole-20180305T164528'
bucket = 's3://kono-trained-models'

image = '763104351884.dkr.ecr.ap-northeast-2.amazonaws.com/tensorflow-inference:1.14-gpu'
instance_type = 'ml.t2.medium'

conf = {
    "env": 'nlu-shield-staging',
    "user_profile": "kono-deployment",
    "title_link": "http://nlu-shield.kono.ai/",
    "DJANGO_SETTINGS_MODULE": "main.settings_test",
}


def pycodestyle():
    local("pycodestyle --config=setup.cfg")
    local("flake8 --config=setup.cfg")
    local("vulture bert/* --exclude=\"*/migrations/*\" --min-confidence 90")


def test_code(reuse_db=0, case=''):
    pycodestyle()


def sagemaker(action='new', model_name='models.tar.gz'):
    session = boto3.Session()
    client = session.client('sagemaker')

    if action == 'new':
        model_path = "%s/%s" % (bucket, model_name)
        model = Model(image=image, model_data=model_path, role=role)
        predictor = model.deploy(initial_instance_count=1,
                                 instance_type=instance_type)

        print("\n%s" % predictor.endpoint)

    elif action == 'desc':
        endpoints = client.list_endpoints()

        for ep in endpoints['Endpoints']:
            print("{EndpointName} - {EndpointStatus}".format(**ep))

    elif action == 'del':
        endpoints = client.list_endpoints()

        for ep in endpoints['Endpoints']:
            print("{EndpointName} - {EndpointStatus}".format(**ep))
            response = client.delete_endpoint(EndpointName=ep['EndpointName'])
            print(response)

    else:
        print('no possible options')


def deploy_playground():
    with prefix("cd ../playground/"):
        local("yarn build")
        local("aws s3 cp build s3://playground.kono.ai/  --profile kono-hyunmin --recursive")


class NluDeployment:
    def deploy(self, force=False, env='staging-kono-bot', test=True, notice=True):

        if test is True:
            test_code()

        local("eb deploy {env} --profile {user_profile}".format(**conf))


ld = NluDeployment()


def nlu_deploy(force=False, env='staging-kono-bot', test=True, notice=True):
    ld.deploy(force=force, env=env, test=test, notice=notice)
