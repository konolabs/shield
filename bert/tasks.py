from __future__ import absolute_import, unicode_literals

from celery import shared_task

from bert.utils.helpers import fetch_user_feedback, preserve_user_feedback_s3
from bert.utils.notice import send_slack_message


@shared_task
def done_daily_backup():
    items = fetch_user_feedback()
    res, key = preserve_user_feedback_s3(items)
    if res['ResponseMetadata']['HTTPStatusCode'] == 200:
        send_slack_message(text='Preserving on S3 has done', message='%s (%d)' % (key, len(items)))
