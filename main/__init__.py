from __future__ import absolute_import, unicode_literals

from django.utils.version import get_version

from .shield_celery import app as celery_app

VERSION = (0, 0, 1, 'alpha', 0)
__version__ = get_version(VERSION)
__all__ = ['celery_app']
