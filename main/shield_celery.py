from __future__ import absolute_import, unicode_literals

import os

import raven
from celery import Celery
from raven.contrib.celery import register_logger_signal, register_signal

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'main.settings')


# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
RAVEN_CONFIG_DSN = os.getenv(
    "RAVEN_CONFIG_DSN",
    "https://fe7e0d9d8e40465496e4ea01223d4049:1ad834d6ac584a26881cd41c07ae7db2@sentry.io/1204865")


class ShieldCelery(Celery):
    def on_configure(self):
        client = raven.Client(RAVEN_CONFIG_DSN)
        register_logger_signal(client)
        register_signal(client)


app = ShieldCelery(main='nlu-shield')


app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()
