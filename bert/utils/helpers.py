import collections
import json
import os
from datetime import timedelta

import boto3
from django.utils import timezone

import tensorflow as tf

from . import tokenization
from .run_squad import (RawResult, SquadExample, _compute_softmax,
                        convert_examples_to_features, get_final_text)

client_dynamodb = boto3.client('dynamodb', region_name='us-west-1')
client_s3 = boto3.client('s3', region_name='us-west-1')

tokenizer = tokenization.FullTokenizer(vocab_file='bert/data/vocab.txt', do_lower_case=False)
output_dir = '/tmp/'


def read_squad_json_examples(input_data):
  """Read a SQuAD file into a list of SquadExample."""

  def is_whitespace(c):
    if c == " " or c == "\t" or c == "\r" or c == "\n" or ord(c) == 0x202F:
      return True
    return False

  examples = []
  for entry in input_data:
    for paragraph in entry["paragraphs"]:
      paragraph_text = paragraph["context"]
      doc_tokens = []
      char_to_word_offset = []
      prev_is_whitespace = True
      for c in paragraph_text:
        if is_whitespace(c):
          prev_is_whitespace = True
        else:
          if prev_is_whitespace:
            doc_tokens.append(c)
          else:
            doc_tokens[-1] += c
          prev_is_whitespace = False
        char_to_word_offset.append(len(doc_tokens) - 1)

      for qa in paragraph["qas"]:
        qas_id = qa["id"]
        question_text = qa["question"]
        start_position = None
        end_position = None
        orig_answer_text = None
        is_impossible = False

        example = SquadExample(
            qas_id=qas_id,
            question_text=question_text,
            doc_tokens=doc_tokens,
            orig_answer_text=orig_answer_text,
            start_position=start_position,
            end_position=end_position,
            is_impossible=is_impossible)
        examples.append(example)

  return examples


def process_inputs(input_json):
    eval_examples = read_squad_json_examples(input_json)
    eval_features = []

    def append_feature(feature):
        eval_features.append(feature)

    convert_examples_to_features(
        examples=eval_examples,
        tokenizer=tokenizer,
        max_seq_length=128,
        doc_stride=128,
        max_query_length=64,
        is_training=False,
        output_fn=append_feature)

    return eval_examples, eval_features


def features_to_string(features):

  def create_int_feature(values):
    feature = tf.train.Feature(
        int64_list=tf.train.Int64List(value=list(values)))
    return feature

  string_features = []
  for feature in features:

    features = collections.OrderedDict()
    features["unique_ids"] = create_int_feature([feature.unique_id])
    features["input_ids"] = create_int_feature(feature.input_ids)
    features["input_mask"] = create_int_feature(feature.input_mask)
    features["segment_ids"] = create_int_feature(feature.segment_ids)

    tf_example = tf.train.Example(features=tf.train.Features(feature=features))

    string_features.append(tf_example.SerializeToString())
  return string_features


def _get_best_indexes(logits, n_best_size):
  """Get the n-best logits from a list."""
  index_and_score = sorted(enumerate(logits), key=lambda x: x[1], reverse=True)

  best_indexes = []
  for i in range(len(index_and_score)):
    if i >= n_best_size:
      break
    best_indexes.append(index_and_score[i][0])
  return best_indexes


def get_predictions(all_examples, all_features, all_results, n_best_size,
                    max_answer_length, do_lower_case, output_prediction_file,
                    output_nbest_file, output_null_log_odds_file):
  """Write final predictions to the json file and log-odds of null if needed."""
  tf.logging.info("Writing predictions to: %s" % (output_prediction_file))
  tf.logging.info("Writing nbest to: %s" % (output_nbest_file))

  example_index_to_features = collections.defaultdict(list)
  for feature in all_features:
    example_index_to_features[feature.example_index].append(feature)
  unique_id_to_result = {}

  if 'predictions' in all_results:
    for result in all_results['predictions']:
      unique_id_to_result[result['unique_ids']] = result

  else:
    for result in all_results:
      unique_id_to_result[result.unique_id] = result

  _PrelimPrediction = collections.namedtuple(  # pylint: disable=invalid-name
      "PrelimPrediction",
      ["feature_index", "start_index", "end_index", "start_logit", "end_logit"])

  all_predictions = collections.OrderedDict()
  all_nbest_json = collections.OrderedDict()

  for (example_index, example) in enumerate(all_examples):
    features = example_index_to_features[example_index]

    prelim_predictions = []
    # keep track of the minimum score of null start+end of position 0

    for (feature_index, feature) in enumerate(features):
      result = unique_id_to_result[feature.unique_id]

      start_logits = result.get('start_logits')
      end_logits = result.get('end_logits')

      start_indexes = _get_best_indexes(start_logits, n_best_size)
      end_indexes = _get_best_indexes(end_logits, n_best_size)

      for start_index in start_indexes:
        for end_index in end_indexes:
            # We could hypothetically create invalid predictions, e.g., predict
            # that the start of the span is in the question. We throw out all
            # invalid predictions.
          if start_index >= len(feature.tokens):
            continue
          if end_index >= len(feature.tokens):
            continue
          if start_index not in feature.token_to_orig_map:
            continue
          if end_index not in feature.token_to_orig_map:
            continue
          if not feature.token_is_max_context.get(start_index, False):
            continue
          if end_index < start_index:
            continue
          length = end_index - start_index + 1
          if length > max_answer_length:
            continue
          prelim_predictions.append(
              _PrelimPrediction(
                  feature_index=feature_index,
                  start_index=start_index,
                  end_index=end_index,
                  start_logit=start_logits[start_index],
                  end_logit=end_logits[end_index]))

    prelim_predictions = sorted(
        prelim_predictions,
        key=lambda x: (x.start_logit + x.end_logit),
        reverse=True)

    _NbestPrediction = collections.namedtuple(  # pylint: disable=invalid-name
        "NbestPrediction", ["text", "start_logit", "end_logit"])

    seen_predictions = {}
    nbest = []
    for pred in prelim_predictions:
      if len(nbest) >= n_best_size:
        break
      feature = features[pred.feature_index]
      if pred.start_index > 0:  # this is a non-null prediction
        tok_tokens = feature.tokens[pred.start_index:(pred.end_index + 1)]
        orig_doc_start = feature.token_to_orig_map[pred.start_index]
        orig_doc_end = feature.token_to_orig_map[pred.end_index]
        orig_tokens = example.doc_tokens[orig_doc_start:(orig_doc_end + 1)]
        tok_text = " ".join(tok_tokens)

        # De-tokenize WordPieces that have been split off.
        tok_text = tok_text.replace(" ##", "")
        tok_text = tok_text.replace("##", "")

        # Clean whitespace
        tok_text = tok_text.strip()
        tok_text = " ".join(tok_text.split())
        orig_text = " ".join(orig_tokens)

        final_text = get_final_text(tok_text, orig_text, do_lower_case)
        if final_text in seen_predictions:
          continue

        seen_predictions[final_text] = True
      else:
        final_text = ""
        seen_predictions[final_text] = True

      nbest.append(
          _NbestPrediction(
              text=final_text,
              start_logit=pred.start_logit,
              end_logit=pred.end_logit))

    if not nbest:
      nbest.append(
          _NbestPrediction(text="empty", start_logit=0.0, end_logit=0.0))

    assert len(nbest) >= 1

    total_scores = []
    best_non_null_entry = None
    for entry in nbest:
      total_scores.append(entry.start_logit + entry.end_logit)
      if not best_non_null_entry:
        if entry.text:
          best_non_null_entry = entry

    probs = _compute_softmax(total_scores)

    nbest_json = []
    for (i, entry) in enumerate(nbest):
      output = collections.OrderedDict()
      output["text"] = entry.text
      output["probability"] = probs[i]
      output["start_logit"] = entry.start_logit
      output["end_logit"] = entry.end_logit
      nbest_json.append(output)

    assert len(nbest_json) >= 1

    all_predictions[example.qas_id] = nbest_json[0]["text"]
    all_nbest_json[example.qas_id] = nbest_json

  with tf.gfile.GFile(output_prediction_file, "w") as writer:
    writer.write(json.dumps(all_predictions, indent=4) + "\n")

  with tf.gfile.GFile(output_nbest_file, "w") as writer:
    writer.write(json.dumps(all_nbest_json, indent=4) + "\n")

  return all_predictions, all_nbest_json


def process_result(result):
    formatted_results = []
    unique_ids = result['unique_ids'].int64_val
    for idx, unique_id in enumerate(unique_ids):
        idx_start = idx * 128
        idx_end = (idx + 1) * 128
        start_logits = [float(x) for x in result["start_logits"].float_val[idx_start:idx_end]]
        end_logits = [float(x) for x in result["end_logits"].float_val[idx_start:idx_end]]

        formatted_result = RawResult(unique_id=unique_id, start_logits=start_logits,
                                     end_logits=end_logits)

        formatted_results.append(formatted_result)

    return formatted_results


def process_output(all_results, eval_examples, eval_features,
                   input_data, n_best, n_best_size, max_answer_length):

    output_prediction_file = os.path.join(output_dir, "hm_squad_predictions.json")
    output_nbest_file = os.path.join(output_dir, "hm_squad_nbest_predictions.json")
    output_null_log_odds_file = os.path.join(output_dir, "hm_squad_null_odds.json")

    all_predictions, all_nbest_json = get_predictions(
        eval_examples, eval_features, all_results,
        n_best_size=n_best_size, max_answer_length=max_answer_length,
        do_lower_case=False, output_prediction_file=output_prediction_file,
        output_nbest_file=output_nbest_file, output_null_log_odds_file=output_null_log_odds_file)

    res = []
    for i in range(len(all_predictions)):
        id_ = eval_examples[i].qas_id
        if n_best:
            res.append(collections.OrderedDict({
                "id": id_,
                "question": eval_examples[i].question_text,
                "best_prediction": all_predictions[id_],
                "n_best_predictions": all_nbest_json[id_]
            }))
        else:
            res.append(collections.OrderedDict({
                "id": id_,
                "question": eval_examples[i].question_text,
                "best_prediction": all_predictions[id_]
            }))

    return json.loads(json.dumps(res))


def preserve_user_feedback(**kwargs):
  now = timezone.localtime()
  today = now.strftime("%Y%m%d")

  res = kwargs.get('res', '')
  context = kwargs.get('context', '')

  item = {'created_dt': {"S": today},
          'created_at': {"S": str(now)},
          'context': {"S": context},
          'predict': {"B": json.dumps(res, ensure_ascii=False).encode('utf8')}}

  ret = client_dynamodb.put_item(TableName='korquad', Item=item)

  return ret


def fetch_user_feedback(now=timezone.localtime()):
  """ input: now time
      outout: list of items on dynamodb
  """
  today = now.strftime("%Y%m%d")
  yesterday = now - timedelta(days=1)

  res = client_dynamodb.query(TableName='korquad',
                              KeyConditionExpression="#D = :created_dt and #A > :created_at",
                              ExpressionAttributeNames={"#A": "created_at", "#D": "created_dt"},
                              ExpressionAttributeValues={":created_at": {"S": str(yesterday)},
                                                         ":created_dt": {"S": today}})

  for ele in res['Items']:
    if 'predict' in ele and 'B' in ele['predict'] and isinstance(ele['predict']['B'], bytes):
      ele['predict']['B'] = json.loads(ele['predict']['B'])

  return res['Items']


def preserve_user_feedback_s3(items, now=timezone.localtime()):
  yesterday, year = (now - timedelta(days=1)).strftime("%Y%m%d"), now.strftime("%Y")
  key = "nlu-shield/{year}/{yesterday}.json".format(**dict(year=year, yesterday=yesterday))
  res = client_s3.put_object(Bucket='konobot-logs', Key=key, Body=json.dumps(items))
  return res, key
