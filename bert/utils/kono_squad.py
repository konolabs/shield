import base64
import json

import boto3
from constance import config

import tensorflow as tf

from .helpers import (features_to_string, process_inputs, process_output)

batch_size = 10


def predict(data, n_best=True):
    exams, feats = process_inputs(data['data'])
    string_features = features_to_string(feats)

    tensor_proto = tf.contrib.util.make_tensor_proto(string_features, dtype=tf.string, shape=[batch_size])
    tensor_proto_array = tf.contrib.util.make_ndarray(tensor_proto)

    session = boto3.Session()
    client = session.client('sagemaker-runtime', region_name='ap-northeast-2')

    custom_attributes = "c000b4f9-df62-4c85-a0bf-7c525f9104a4"

    endpoint_name = config.ENDPOINT_NAME
    content_type = "application/json"
    payload = {"signature_name": "serving_default"}

    lst = []
    for ele in tensor_proto_array:
        b64 = base64.b64encode(ele).decode('utf-8')
        examples = dict(examples=dict(b64=b64))
        lst.append(examples)

    payload['instances'] = lst
    response = client.invoke_endpoint(EndpointName=endpoint_name,
                                      CustomAttributes=custom_attributes,
                                      ContentType=content_type, Body=json.dumps(payload))
    body = response['Body'].read()
    output = json.loads(body)

    result = process_output(output, exams, feats, input_data=[], n_best=n_best, n_best_size=3, max_answer_length=30)

    return result
